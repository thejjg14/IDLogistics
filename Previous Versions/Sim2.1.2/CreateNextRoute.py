import CreateRoute


def CreateNextRoute(agent, vmap):

    # done to avoid an error inside A* "'int' object is not subscriptable"
    # in: return np.sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2)

    start = (agent._pos[0], agent._pos[1])
    print("Agent picks", agent._picks)
    goal = (
        agent._picks[1][0],
        agent._picks[1][1],
    )  # [1] must be set in picks. [0] is the current position of the robot.

    agent.set_route(CreateRoute(vmap, start, goal))

    # add the pick position to the path vector
    # has to be done with a 'for' because otherwise it would add the whole vector in the first path position

    return
