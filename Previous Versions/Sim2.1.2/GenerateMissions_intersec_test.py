from random import randint
from m_CTF import m_CTF
from CreatePicks_intersec_test import CreatePicks
from CreateRoute import CreateRoute


def GenerateMissions(
    vmap, map_list, xmap, robots, mission, picks_Mission, demand, t, len_x
):
    num_picks = randint(picks_Mission[0], picks_Mission[1])
    # Get robots class process
    selected_robot = -1
    available_robots = 0

    for i in range(0, len(robots)):
        if robots[i].get_process() == 0:
            available_robots = available_robots + 1
            selected_robot = i

    if available_robots == 0:
        return 0

    elif available_robots > 0:
        mission.set_robotassigned(robotassigned=selected_robot)
        # Amañado para fijar posicion de robot requerida ########################################
        # robots[mission.get_robotassigned()].set_pos((randint(0, len_x), 0))
        if robots[mission.get_robotassigned()].get_ID() == 0:
            robots[mission.get_robotassigned()].set_pos((0, 0))
        elif robots[mission.get_robotassigned()].get_ID() == 1:
            robots[mission.get_robotassigned()].set_pos((len_x, 0))
        else:
            robots[mission.get_robotassigned()].set_pos((randint(0, len_x), 0))
        CreatePicks(
            vmap,
            map_list,
            mission,
            robots[mission.get_robotassigned()].get_pos(),
            num_picks,
            demand,
        )
        robots[mission._robotassigned].set_assignedmission(mission.get_ID())
        robots[mission._robotassigned].set_process(1)
        goal = tuple(mission.get_picks()[0].get_pos())
        start = tuple(robots[mission._robotassigned].get_pos())
        robots[mission._robotassigned].set_route(CreateRoute(vmap, start, goal))
        mission.set_process(1)
        robots[mission.get_robotassigned()].set_process(1)
        mission.set_tstart(t)

        return 1
