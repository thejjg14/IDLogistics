from CreatePicks import CreatePicks
from OrderPicks import OrderPicks
from GenerateMissions_intersec_test import GenerateMissions
from Robot import Robot
from Pick import Pick
from Mission import Mission
from MoveRobot import MoveRobot
from m_CTF import m_CTF


import copy
import numpy as np
from matplotlib import pyplot
from matplotlib import colors


def check(robots, mission):
    value = False

    for x in range(len(mission)):
        value = value or (
            robots[mission[x].get_robotassigned()].get_pos()
            != mission[x].get_picks()[0].get_pos()
        )

    return value


t = 0
xmap = 0
vmap = np.zeros((9, 6))
vmap[1, 1:5] = 1
vmap[2, 1:5] = 1
vmap[3, 1:5] = 1
vmap[5, 1:5] = 1
vmap[6, 1:5] = 1
vmap[7, 1:5] = 1
map_list = vmap.tolist()
vmap_0 = copy.deepcopy(vmap)
map_list_0 = copy.deepcopy(map_list)
num_robots = 2
num_missions = 2
num_picks_min = 2
num_picks_max = 2
robots = []
robots = [Robot(x) for x in range(0, num_robots)]
mission = [Mission(x) for x in range(0, num_missions)]
for x in range(0, num_missions):
    GenerateMissions(
        vmap,
        map_list,
        xmap,
        robots,
        mission[x],
        [num_picks_min, num_picks_max],
        0,
        t,
        len(map_list) - 1,
    )

t = 0

vmap_2 = copy.deepcopy(vmap_0)
vmap_route = copy.deepcopy(vmap_0)
map_list_2 = copy.deepcopy(map_list_0)

for x in range(len(mission)):
    vmap_2[
        robots[mission[x].get_robotassigned()].get_pos()[0],
        robots[mission[x].get_robotassigned()].get_pos()[1],
    ] = 2
vmap = copy.deepcopy(vmap_2)

while check(robots, mission):

    vmap_2 = copy.deepcopy(vmap_0)
    vmap_route = copy.deepcopy(vmap_0)
    map_list_2 = copy.deepcopy(map_list_0)

    for x in range(len(mission)):
        vmap_2[
            robots[mission[x].get_robotassigned()].get_pos()[0],
            robots[mission[x].get_robotassigned()].get_pos()[1],
        ] = 2
    vmap = copy.deepcopy(vmap_2)

    map_list_2 = vmap_2.tolist()

    colormap = colors.ListedColormap(["white", "grey", "black"])
    pyplot.imshow(map_list_2, cmap=colormap)
    pyplot.show()

    for mission_i in mission:
        if (
            robots[mission_i.get_robotassigned()].get_pos()
            != mission_i.get_picks()[0].get_pos()
        ):
            MoveRobot(
                robots[mission_i.get_robotassigned()],
                robots,
                mission_i.get_picks()[0].get_pos(),
                vmap,
                copy.deepcopy(vmap_0),
                mission_i,
            )
        else:
            # Eliminar el robot del mapa
            print("Mission ", mission_i.get_ID, " completada")

    """colormap = colors.ListedColormap(["white", "grey", "black"])
    pyplot.imshow(map_list_2, cmap=colormap)
    pyplot.show()"""

    t = t + 1

for x in mission:
    print(robots[x.get_robotassigned()].get_dist())
