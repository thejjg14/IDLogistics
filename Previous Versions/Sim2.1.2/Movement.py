from CreatePos import CreatePos
from OrderPicks import OrderPicks
from m_CTF import m_CTF  # Import m_CTF function from m_CTF.py file
from m_SPF import m_SPF  # Import m_SPF function from m_SPF.py file
from CreateRoute import (
    CreateRoute,
)  # Import CreateRoute function from CreateRoute.py file
from MoveRobot import MoveRobot

import random

import copy


def Movement(mission, robot, human, xmap):
    if xmap == 0:
        [vmap, map_list] = m_CTF()
    elif xmap == 1:
        [vmap, map_list] = m_SPF
    process = mission.get_process()

    if mission.get_process() == 0:

        a = 0

    if mission.get_process() == 1:

        if (
            robot.get_pos() == mission.get_picks()[0].get_pos()
        ):  # If robot is at pick location

            mission.set_process(process=2)  # Set mission process to 2

        if (
            robot.get_pos() != mission.get_picks()[0].get_pos()
        ):  # If robot is not at pick location

            MoveRobot(
                robot, mission.get_picks()[0], vmap, mission.get_picks()[0]
            )  # Move robot to pick location

    if mission.get_process() == 2:

        # Get robots class process
        selected_human = -1  # Set selected human to -1
        available_humans = 0  # Number of humans available to pick

        for i in range(0, len(human)):
            if human[i].get_process() == 0:  # If human is available
                available_humans = available_humans + 1  # Add 1 to available humans
                selected_human = i  # Set selected human to i

        if available_humans == 0:  # If no humans are available
            return 0

        elif available_humans > 0:  # If humans are available

            mission.set_humanassigned(humanassigned=selected_human)
            human[mission.get_humanassigned()].set_process(
                process=1
            )  # Set human process to 1
            human[mission.get_humanassigned()].set_goal(
                goal=mission.get_picks()[0].get_pos()
            )  # Set human goal to pick location
            human[mission.get_humanassigned()].set_deb_route(
                deb_route=CreateRoute(
                    vmap,
                    human[mission.get_humanassigned()].get_pos(),
                    human[mission.get_humanassigned()].get_goal(),
                )  # Set human route to route from human position to pick location
            )
            human[mission.get_humanassigned()].set_dist(
                len(human[mission.get_humanassigned()].get_deb_route())
            )  # Set human distance to length of route

            mission.get_picks()[0].set_waittime(
                waittime=human[mission.get_humanassigned()].get_dist() * 1.4
            )  # Add 1 to human wait time

            human[mission.get_humanassigned()].set_assignedmission(
                assignedmission=mission.get_ID()
            )  # Set human assigned mission to mission id

            # Hay que desarrollar la clase picks
            mission.get_picks()[0].set_waittime(
                human[mission.get_humanassigned()].get_dist()
                / human[mission.get_humanassigned()].get_vel()
            )
            mission.get_picks()[0].set_waittime(waittime=0)

            mission.set_process(process=3)

    if mission.get_process() == 3:

        if (
            mission.get_picks()[0].get_waittime()
            < mission.get_picks()[0].get_waittime()
        ):  # If wait time is less than human wait time
            mission.get_picks()[0].set_waittime(
                waittime=mission.get_picks()[0].get_waittime() + 1.4
            )  # Add 1.4 to wait time

        if (
            mission.get_picks()[0].get_waittime()
            >= mission.get_picks()[0].get_waittime()
        ):  # If wait time is greater than human wait time
            mission.set_process(process=4)  # Set mission process to 4
            human[mission.get_humanassigned()].set_process(
                process=2
            )  # Set human process to 2
            mission.get_picks()[0].set_picktime(picktime=0)  # Set pick time to 0

    if mission.get_process() == 4:

        if (
            mission.get_picks()[0].get_picktime() < human.get_actualpicktime()
        ):  # If pick time is less than human actual pick time
            mission.get_picks()[0].set_picktime(
                picktime=mission.get_picks()[0].get_picktime() + 1.4
            )  # Add 1.4 to pick time

        if (
            mission.get_picks()[0].get_picktime()
            >= human[mission.get_humanassigned()].get_picktime()
        ):  # If pick time is greater than human pick time

            human[mission.get_humanassigned()].set_process(
                process=0
            )  # Set human process to 0
            human[mission.get_humanassigned()].set_deb_numpicks(
                deb_numpicks=human.get_deb_numpicks() + 1
            )  # Add 1 to human number of picks
            human[mission.get_humanassigned()].set_deb_route(
                deb_route=[]
            )  # Set human route to empty list
            human[mission.get_humanassigned()].set_dist(
                dist=0
            )  # Set human distance to 0
            human[mission.get_humanassigned()].set_assignedmission(
                assignedmission=-1
            )  # Set human assigned mission to -1
            human[mission.get_humanassigned()].set_goal(
                goal=[]
            )  # Set human goal to empty list
            human[mission.get_humanassigned()].set_actualpicktime(
                actualpicktime=0
            )  # Set human actual pick time to 0

            mission.set_numpicks(
                numpicks=mission.get_numpicks() + 1
            )  # Add 1 to mission number of picks
            mission.set_picks(
                picks=mission.get_picks().pop(0)
            )  # Remove first pick from mission picks list
            mission.set_humanassigned(
                humanassigned=-1
            )  # Set mission human assigned to 0

            if len(mission.get_picks()) == 0:  # If mission picks list is empty

                mission.set_process(process=5)  # Set mission process to 5

                if xmap == 1:  # If SPF
                    zonadescarga = random.randint(64, 153)
                    mission.set_goal(goal=(49, zonadescarga))

                    robot.set_route(
                        route=CreateRoute(vmap, robot.get_pos(), mission.get_goal())
                    )

                    mission.set_deb_route(
                        deb_route=mission.get_deb_route.append(robot.get_route())
                    )

                if xmap == 0:  # If CTF
                    zonadescarga = random.randint(40, 115)
                    mission.set_goal(goal=(63, zonadescarga))
                    robot.set_route(
                        route=CreateRoute(vmap, robot.get_pos(), mission.get_goal())
                    )

                    mission.set_deb_route(
                        deb_route=mission.get_deb_route().append(robot.get_route())
                    )

                robot[mission.get_robotassigned].set_process(
                    1
                )  # Set robot process to 1

                mission.get_picks()[0].set_waittime(
                    waittime=0
                )  # Set pick wait time to 0"""

            if len(mission.get_picks()) > 0:  # If mission picks list is not empty

                mission.set_process(process=1)  # Set mission process to 1

                robot.set_goal(goal=mission.get_picks()[0])

                robot.set_route(
                    route=CreateRoute(vmap, robot.get_pos(), robot.get_goal())
                )

                mission.set_deb_route(
                    deb_route=mission.get_deb_route().append(robot.get_route())
                )

    if mission.get_process() == 5:

        if robot.get_pos() == mission.get_picks()[0]:

            mission.set_process(process=6)

        if robot.get_pos() != mission.get_picks()[0]:

            MoveRobot(robot, mission.get_picks()[0], vmap, [])

    if mission.get_process() == 6:

        # Llamar a csv

        robot.set_goal(goal=[])
        robot.set_route(route=[])
        robot.set_process(process=0)
        robot.set_assignedmission(assignedmission=0)
        robot.set_pos(pos=[])

        mission.set_process(process=7)

    if process == 7:

        return

    return
