from abc import ABC


class Pick(ABC):
    def _init_(self, ID: int):
        self._missionID = ID  # Mission ID
        self._pickID = ID  # Pick ID
        self._pos: tuple  # Picks position
        self._transittime: float = 0.0  # Picks transit time
        self._waittime: float = 0.0  # Picks wait time
        self._intersections: int = 0  # Picks intersections

    def set_missionID(self, missionID: int):
        self._missionID = missionID

    def get_missionID(self):
        return self._missionID

    def set_pickID(self, pickID: int):
        self._pickID = pickID

    def get_pickID(self):
        return self._pickID

    def set_pos(self, pos: tuple):
        self._pos = pos

    def get_pos(self):
        return self._pos

    def set_transittime(self, transittime: float):
        self._transittime = transittime
