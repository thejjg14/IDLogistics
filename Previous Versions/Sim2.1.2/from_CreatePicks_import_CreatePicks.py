from CreatePicks import CreatePicks
from OrderPicks import OrderPicks
from GenerateMissions import GenerateMissions
from Robot import Robot
from Pick import Pick
from Mission import Mission
from MoveRobot import MoveRobot
from m_CTF import m_CTF

import copy
import numpy as np
from matplotlib import pyplot
from matplotlib import colors


def check(robots, mission):
    value = False

    for x in range(len(mission)):
        value = value or (
            robots[mission[x].get_robotassigned()].get_pos()
            != mission[x].get_picks()[0].get_pos()
        )

    return value


t = 0
xmap = 0
[vmap, map_list] = m_CTF()
[vmap_0, map_list_0] = m_CTF()
num_robots = 50
num_missions = 40
num_picks_min = 2
num_picks_max = 3
robots = []
robots = [Robot(x) for x in range(0, num_robots)]
mission = [Mission(x) for x in range(0, num_missions)]
for x in range(0, num_missions):
    GenerateMissions(
        vmap, map_list, xmap, robots, mission[x], [num_picks_min, num_picks_max], 0, t
    )

t = 0

vmap_2 = copy.deepcopy(vmap_0)
vmap_route = copy.deepcopy(vmap_0)
map_list_2 = copy.deepcopy(map_list_0)

for x in range(len(mission)):
    vmap_2[
        robots[mission[x].get_robotassigned()].get_pos()[0],
        robots[mission[x].get_robotassigned()].get_pos()[1],
    ] = 2
vmap = copy.deepcopy(vmap_2)

while check(robots, mission):
    print("Turno ", t)

    vmap_2 = copy.deepcopy(vmap_0)
    map_list_2 = copy.deepcopy(map_list_0)

    for mission_i in mission:
        if (
            robots[mission_i.get_robotassigned()].get_pos()
            != mission_i.get_picks()[0].get_pos()
        ):
            MoveRobot(
                robots[mission_i.get_robotassigned()],
                robots,
                mission_i.get_picks()[0].get_pos(),
                vmap,
                vmap_2,
                mission_i,
            )

        else:
            # Eliminar el robot del mapa
            print("Mission ", mission_i.get_ID(), " completada")

    for x in range(len(mission)):
        vmap_2[
            robots[mission[x].get_robotassigned()].get_pos()[0],
            robots[mission[x].get_robotassigned()].get_pos()[1],
        ] = 2
    vmap = copy.deepcopy(vmap_2)

    map_list_2 = vmap_2.tolist()

    colormap = colors.ListedColormap(["white", "grey", "black"])
    pyplot.imshow(map_list_2, cmap=colormap)
    pyplot.show()

    print("\n")

    t = t + 1

for x in mission:
    print(robots[mission.get_robotassigned()].get_dist())
