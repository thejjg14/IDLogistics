from abc import ABC
from random import randint
from Robot import Robot  # Import Robot class from Robot.py file
from Human import Human  # Import Human class from Human.py file
from Mission import Mission  # Import Mission class from Mission.py file
from m_CTF import m_CTF  # Import m_CTF function from m_CTF.py file
from m_SPF import m_SPF  # Import m_SPF function from m_SPF.py file
from CreateRoute import (
    CreateRoute,
)  # Import CreateRoute function from CreateRoute.py file
from CreatePos import CreatePos  # Import CreatePos function from CreatePos.py file
from OrderPicks import OrderPicks  # Import OrderPicks function from OrderPicks.py file
from CreatePicks import (
    CreatePicks,
)  # Import CreatePicks function from CreatePicks.py file
from GenerateMissions import GenerateMissions
from Movement import Movement

# Main

# Declaration of variables included in interface

# We declare map depending on the one introduced in the interface
# The variable array of the map has been created before.

xmap = 0  # 0: CTF, 1: SPF

if xmap == 0:
    vmap, map_list = m_CTF()  # Load the CTF map

elif xmap == 1:
    vmap, map_list = m_SPF()  # Load the SPF map


# vmap = map variable

# Declaration of variables entered in interface

Num_Robots = 20  # Number of robots
Num_Human = 5  # Number of humans

High_demand_days = 5
Low_demand_days = 5
Sim_days = High_demand_days + Low_demand_days

Sim_s = Sim_days * 60 * 60 * 24
High_demand_s = High_demand_days * 60 * 60 * 24
Low_demand_s = Low_demand_days * 60 * 60 * 24

Wave_time = 0.5 * 60 * 60 * 24  # Time between each wave

Missions_Wave = [5, 10]  # Missions per wave

Picks_Mission = [2, 5]  # Range of picks per tote

Missions_left = 0  # Remaining missions that have not been assigned

Num_Missions = 0  # Number of total missions


# Initialization of time variables

t = 0.0  # Total simulation time
tday = 0.0  # Time of day (0s to 86400s)
# Day zone (0 = day, 1 = evening, 2 = night) (0s-28800s, 28800s-57600s, 57600s-86400s)
zday = 0
twave = 0.0  # Wave time (0s to timeola s)
tmov = 0.0  # Robot and Human movement time

# Simulation execution time (only used for testing and printing, to be removed later)
texecution = 0.0

# Function declares instances depending on variables entered by user

robots = []
robots = [Robot(x) for x in range(0, Num_Robots)]

humans = []
humans = [Human(x, 1, 15) for x in range(0, Num_Human)]

missions = []

# We initialize humans at a random point on the map.
for i in range(Num_Human):
    humans[i]._pos = (
        randint(0, len(map_list)),
        randint(0, len(map_list[0])),
    )
Missions_Assigned = 1
while t <= Sim_s:
    if t < High_demand_s:  # If we are in a period of high demand
        demand = 1

    elif t > High_demand_s:  # If we are in a period of low demand
        demand = 0

    if twave >= Wave_time:
        twave = 0.0

        # Generate wave

        num_mis_gen = randint(
            Missions_Wave[0], Missions_Wave[1]
        )  # Number of missions generated in each wave

        Missions_left = Missions_left + num_mis_gen
        Num_Missions = Num_Missions + num_mis_gen
    ret = 1
    while Missions_left > 0 and ret == 1:  # If Missions remain to be assigned

        # Assign missions
        mission = Mission(Missions_Assigned)
        ret = GenerateMissions(
            vmap, map_list, xmap, robots, mission, Picks_Mission, demand, t
        )  # Generate the missions

        if ret == 1:
            Missions_left = Missions_left - 1
            missions.append(mission)
            Missions_Assigned = Missions_Assigned + 1
    for i in range(0, len(missions)):
        Movement(
            missions[i],
            missions,
            robots[missions[i].get_robotassigned()],
            robots,
            humans,
            xmap,
        )
    j = 0
    while j < len(missions):
        if missions[j].get_process() == 7:
            missions.pop(j)
        else:
            j = j + 1

    # Time Management

    if tday > 86400:  # If we have passed a day
        tday = 0  # Reset the time of day
        zday = 0  # Reset the time zone of the day to tomorrow

    # Update time variables and round to 1 decimal place (since sometimes it stays at .39999...)

    t = t + 0.7
    round(t, 1)  # Total simulation time
    tmov = tmov + 0.7
    round(tmov, 1)  # Robot and human movement time
    twave = twave + 0.7
    round(twave, 1)  # Wave time
    tday = tday + 0.7
    round(tday, 1)  # Time of day

    texecution = texecution + 0.7
    round(texecution, 1)

    if texecution > 10000:

        print("{:.1f}".format(t))
        texecution = 0
