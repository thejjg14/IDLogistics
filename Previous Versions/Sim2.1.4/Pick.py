from abc import ABC


class Pick(ABC):
    def _init_(self, ID: int):
        self._missionID = ID  # Mission ID
        self._pickID = ID  # Pick ID
        self._pos: tuple  # Picks position
        self._waittime: float = 0.0  # Picks wait time
        self._intersections: int = 0  # Picks intersections
        self._picktime: float = 0.0
        self._actualwaittime: float = 0.0
        self._humantime: float = 0.0

    def set_missionID(self, missionID: int):
        self._missionID = missionID

    def get_missionID(self):
        return self._missionID

    def set_pickID(self, pickID: int):
        self._pickID = pickID

    def get_pickID(self):
        return self._pickID

    def set_intersections(self, intersections: int):
        self._intersections = intersections

    def get_intersections(self):
        return self._intersections

    def set_pos(self, pos: tuple):
        self._pos = pos

    def get_pos(self):
        return self._pos

    def get_waittime(self):
        return self._waittime

    def set_waittime(self, waittime: float):
        self._waittime = waittime

    def get_picktime(self):
        return self._picktime

    def set_picktime(self, picktime: float):
        self._picktime = picktime

    def get_actualwaittime(self):
        return self._actualwaittime

    def set_actualwaittime(self, actualwaittime: float):
        self._actualwaittime = actualwaittime

    def get_humantime(self):
        return self._humantime

    def set_humantime(self, humantime: float):
        self._humantime = humantime
