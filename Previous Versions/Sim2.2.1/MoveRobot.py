import copy
from CreateRoute import CreateRoute


def MoveRobot(robot, goal, vmap, pick):

    a = 0

    while a == 0:

        if len(robot.get_route()) >= 2:

            # If the robot has 2 or more positions in its route, you check both of them

            r00 = robot.get_route()[0][0]
            r01 = robot.get_route()[0][1]
            r10 = robot.get_route()[1][0]
            r11 = robot.get_route()[1][1]

            if vmap[r00][r01] == 0 and vmap[r10][r11] == 0:

                robot.set_pos(pos=robot.get_route()[0])
                route = robot.get_route()
                route.pop(0)
                robot.set_route(route)

                robot.set_dist(dist=robot.get_dist() + 1)

                a = 1

            elif vmap[r00][r01] == 1 or vmap[r10][r11] == 1:

                vmap2 = copy.deepcopy(vmap)
                vmap2[r00][r01] = 1
                vmap2[r10][r11] = 1

                robot.set_route(CreateRoute(vmap2, robot.get_pos(), goal))

                pick.set_intersections(pick.get_intersections() + 1)

        if len(robot.get_route()) == 1:

            # If the robot has 1 position in its route, you check it

            r00 = robot.get_route()[0][0]
            r01 = robot.get_route()[0][1]

            if vmap[r00][r01] == 0:
                robot.set_pos(pos=robot.get_route()[0])
                route = robot.get_route()
                route.pop(0)
                robot.set_route(route)

                robot.set_dist(dist=robot.get_dist() + 1)

                a = 1

            elif vmap[r00][r01] == 1:

                vmap2 = copy.deepcopy(vmap)
                vmap2[r00][r01] = 1

                robot.set_route(CreateRoute(vmap2, robot.get_pos(), goal))

                pick.set_intersections(pick.get_intersections() + 1)

        if len(robot.get_route()) == 0:

            a = 2  # The robot arrived at the pick or it does not have a route (should not happen)

    return a
