from abc import ABC
import array


class Robot(ABC):
    def __init__(self, ID: int, vel: float, pedidos: int, ruta):
        self._ID = ID  # ID del robot
        self._vel: float = vel  # Velocidad del robot
        self._pos = 0  # Posición en matriz array, es un vector
        self._pedidos: int = pedidos  # Número de pedidos máximos del robot
        self._ruta = ruta  # Array de Arrays
        self._vol = 0.0  # Volumen máximo de capacidad
        self._pesomax = 0.0  # Peso máximo de capacidad

        self._proceso: int = 0  # Proceso que se encuentra el robot
        # 0: espera, 1: movimiento, 2: entrega

        self._dist: float = 0.0  # Distancia recorrida por el robot
        self._inter: int = 0  # Intersecciones entre robots encontradas
        self._tiempoespera: float = 0.0  # Tiempo del robot esperando

    def get_ID(self) -> int:
        return self._ID

    def set_ID(self, ID: int):
        self._ID = ID

    def get_vel(self) -> float:
        return self._vel

    def set_vel(self, vel: float):
        if vel > 0:
            self._vel = vel

    def get_pos(self):
        return self._pos

    def set_pos(self, pos):
        self._pos = pos

    def get_pedidos(self) -> int:
        return self._pedidos

    def set_pedidos(self, pedidos: int):
        if pedidos == 6 or pedidos == 12:
            self._pedidos = pedidos

    """def get_vol(self) -> float:
        return self._vol

    def set_vol(self, vol: float):
        if vol > 0:
            self._vol = vol

    def get_pesomax(self) -> float:
        return self._pesomax

    def set_pesomax(self, pesomax: float):
        if pesomax > 0:
            self._pesomax = pesomax """

    def get_proceso(self) -> int:
        return self._proceso

    def set_proceso(self, proceso: int):
        if proceso >= 0 and proceso <= 3:
            self._proceso = proceso

    def get_ruta(self) -> array.array:
        return self._ruta

    def set_ruta(self, ruta: array.array):
        self._ruta = ruta

    def get_dist(self) -> float:
        return self._dist

    def set_dist(self, dist: float):
        if dist > 0:
            self._dist = dist

    def get_inter(self) -> int:
        return self._inter

    def set_inter(self, inter: int):
        if inter > 0:
            self._inter = inter

    def get_tiempoespera(self) -> float:
        return self._tiempoespera

    def set_tiempoespera(self, tiempoespera: float):
        if tiempoespera > 0:
            self._tiempoespera = tiempoespera


class Humano(ABC):
    def __init__(self, ID: int, vel: float, picktime: float):
        self._ID = ID  # ID del humano
        self._pos: float = 0.0  # Posición del humano en matriz
        self._vel: float = 0.0  # Velocidad del humano
        self._picktime: float = 15.0  # Tiempo que tarda en hacer el pick

        self._proceso: int = 0  # Proceso en el que está el humano
        # 0: parado, 1: pedido, 3: picking, 4: regreso

        self._modo: str = "espera"  # Modo en el que está

    def get_ID(self) -> int:
        return self._ID

    def set_ID(self, ID: int):
        self._ID = ID

    def get_vel(self) -> float:
        return self._vel

    def set_vel(self, vel: float):
        if vel > 0:
            self._vel = vel

    def get_pos(self):
        return self._pos

    def set_pos(self, pos):
        self._pos = pos

    def get_picktime(self) -> float:
        return self._picktime

    def set_picktime(self, picktime: float):
        if picktime > 0:
            self._picktime = picktime

    def get_proceso(self) -> int:
        return self._proceso

    def set_proceso(self, proceso: int):
        if proceso >= 0 and proceso <= 4:
            self._proceso = proceso

    def get_modo(self) -> str:
        return self._modo

    def set_modo(self, modo: str):
        self._modo = modo
