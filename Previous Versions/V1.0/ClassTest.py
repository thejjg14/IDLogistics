class Vehicle:
    def __init__(self, manufacturer: str, wheels: int):
        self._manufacturer: str = manufacturer
        self._wheels: int = wheels
        self._speed: float = 20.0

    def get_speed(self) -> float:
        return self._speed

    def set_speed(self, speed: float):
        if speed > 0:
            self._speed = speed

    def get_manufacturer(self) -> str:
        return self._manufacturer

    def get_wheels(self) -> int:
        return self._wheels


if __name__ == "__main__":
    vehicle = Vehicle("SEAT", 4)

    speed = vehicle.get_speed()
    print(speed)
    vehicle.set_speed(50.0)
    speed = vehicle.get_speed()
    print(speed)

    vehicle._speed = -10.0
    print(vehicle._speed)

    manufacturer = vehicle.get_manufacturer()
    print(manufacturer)

    wheels = vehicle.get_wheels()
    print(wheels)
