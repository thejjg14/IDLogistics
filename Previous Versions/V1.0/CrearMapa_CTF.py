import numpy as np
from matplotlib import pyplot
from matplotlib import colors
from matplotlib import axes
import pickle

map_CTF = np.ones((64, 121))

for i in range(map_CTF.shape[1]):
    if (i % 2) == 0:
        map_CTF[:, i] = 0

map_CTF[:3, :] = 0
map_CTF[-5:, :] = 0
map_CTF[-3:, :11] = 1
map_CTF[25:27, :] = 0
map_CTF[41:43, :] = 0
map_CTF[-15:, 29:41] = 0
map_CTF[11:14, 24] = 1
map_CTF[45:48, 24] = 1
map_CTF[11:14, 60] = 1
map_CTF[11:14, 76] = 1
map_CTF[9:12, 75] = 0
map_CTF[14, 75] = 0
map_CTF[11:13, 77] = 0
map_CTF[-9:-6, 52] = 1
map_CTF[-9:-6, 74] = 1
map_CTF[-6, 70:77] = 0
map_CTF[11:14, 104] = 1
map_CTF[45:48, 104] = 1
map_CTF[9:12, 103] = 0
map_CTF[14, 103] = 0
map_CTF[11:13, 105] = 0
map_CTF[-12:-10, 73] = 0
map_CTF[46:49, 103] = 0
map_CTF[43:45, 105] = 0


list_CTF = map_CTF.tolist()

# Escritura en modo binario, vacía el fichero si existe
fichero = open("mapaTarancon_PB.pckl", "wb")

# Escribe la colección en el fichero
pickle.dump(list_CTF, fichero)

fichero.close()

"""
# Lectura en modo binario
fichero = open("mapaTarancon_PB.pckl", "rb")

# Cargamos los datos del fichero
lista_fichero = pickle.load(fichero)

fichero.close()
"""

colormap = colors.ListedColormap(["white", "grey"])

pyplot.imshow(list_CTF, cmap=colormap)

pyplot.show()
