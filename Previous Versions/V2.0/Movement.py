from CreatePos import CreatePos
from OrderPicks import OrderPicks
from m_CTF import m_CTF  # Import m_CTF function from m_CTF.py file
from m_SPF import m_SPF  # Import m_SPF function from m_SPF.py file
from CreateRoute import (
    CreateRoute,
)  # Import CreateRoute function from CreateRoute.py file

import random

import copy


def Movement(mission, robot, human, picks, xmap):

    process = mission.get_process()
    [vmap, map_list] = m_CTF()

    if mission.get_process == 0:

        a = 0

    if mission.get_process == 1:

        if robot.get_pos == mission.get_picks()[0]:  # If robot is at pick location

            mission.set_process(process=2)  # Set mission process to 2

        if robot.get_pos != mission.get_picks()[0]:  # If robot is not at pick location

            MoveRobot(
                robot, mission.get_picks()[0], vmap, picks[0]
            )  # Move robot to pick location

    if mission.get_process == 2:

        # Get robots class process
        selected_human = -1  # Set selected human to -1
        available_humans = 0  # Number of humans available to pick

        for i in range(0, len(human)):
            if human[i].get_process() == 0:  # If human is available
                available_humans = available_humans + 1  # Add 1 to available humans
                selected_human = i  # Set selected human to i

        if available_humans == 0:  # If no humans are available
            return 0

        elif available_humans > 0:  # If humans are available

            mission.set_humanassigned(humanassigned=selected_human)
            human[mission.get_humanassigned].set_process(
                process=1
            )  # Set human process to 1
            human[mission.get_humanassigned].set_goal(
                goal=mission.get_picks()[0]
            )  # Set human goal to pick location
            human[mission.get_humanassigned].set_deb_route(
                deb_route=CreateRoute(
                    vmap, human.get_pos(), human.get_goal()
                )  # Set human route to route from human position to pick location
            )
            human[mission.get_humanassigned].set_dist(
                len(human.get_deb_route())
            )  # Set human distance to length of route

            picks[0].set_humanwaittime(
                humanwaittime=human.get_dist * 1.4
            )  # Add 1 to human wait time

            human[mission.get_humanassigned].set_assignedmission(
                assignedmission=mission.get_id()
            )  # Set human assigned mission to mission id

            # Hay que desarrollar la clase picks
            picks[0].set_humanwaittime(human.get_dist() / human.get_vel())
            picks[0].set_waittime(waittime=0)

            mission.set_process(process=3)

    if mission.get_process == 3:

        if (
            picks[0].get_waittime() < picks[0].get_humanwaittime()
        ):  # If wait time is less than human wait time
            picks[0].set_waittime(
                waittime=picks[0].get_waittime() + 1.4
            )  # Add 1.4 to wait time

        if (
            picks[0].get_waittime() >= picks[0].get_humanwaittime()
        ):  # If wait time is greater than human wait time
            mission.set_process(process=4)  # Set mission process to 4
            human.set_process(process=2)  # Set human process to 2
            picks[0].set_picktime(picktime=0)  # Set pick time to 0

    if mission.get_process == 4:

        if (
            picks[0].get_picktime < human.get_actualpicktime()
        ):  # If pick time is less than human actual pick time
            picks[0].set_picktime(
                picktime=picks[0].get_picktime() + 1.4
            )  # Add 1.4 to pick time

        if (
            picks[0].get_picktime >= human.get_picktime()
        ):  # If pick time is greater than human pick time

            human.set_process(process=0)  # Set human process to 0
            human.set_deb_numpicks(
                deb_numpicks=human.get_deb_numpicks() + 1
            )  # Add 1 to human number of picks
            human.set_deb_route(deb_route=[])  # Set human route to empty list
            human.set_dist(dist=0)  # Set human distance to 0
            human.set_assignedmission(
                assignedmission=-1
            )  # Set human assigned mission to -1
            human.set_goal(goal=[])  # Set human goal to empty list
            human.set_actualpicktime(
                actualpicktime=0
            )  # Set human actual pick time to 0

            mission.set_numpicks(
                numpicks=mission.get_numpicks() + 1
            )  # Add 1 to mission number of picks
            mission.set_picks(
                picks=mission.get_picks().pop(0)
            )  # Remove first pick from mission picks list
            mission.set_humanassigned(
                humanassigned=-1
            )  # Set mission human assigned to 0

            if len(mission.get_picks()) == 0:  # If mission picks list is empty

                mission.set_process(process=5)  # Set mission process to 5

                if xmap == 1:  # If SPF
                    zonadescarga = random.randint(64, 153)
                    mission.set_goal(goal=(49, zonadescarga))

                    robot.set_route(
                        route=CreateRoute(vmap, robot.get_pos(), mission.get_goal())
                    )

                    mission.set_deb_route(
                        deb_route=mission.get_deb_route.append(robot.get_route())
                    )

                if xmap == 0:  # If CTF
                    zonadescarga = random.randint(40, 115)
                    mission.set_goal(goal=(63, zonadescarga))
                    robot.set_route(
                        route=CreateRoute(vmap, robot.get_pos(), mission.get_goal())
                    )

                    mission.set_deb_route(
                        deb_route=mission.get_deb_route.append(robot.get_route())
                    )

                robot[mission.get_robotassigned].set_process(
                    1
                )  # Set robot process to 1

                picks[0].set_waittime(waittime=0)  # Set pick wait time to 0"""

            if len(mission.get_picks()) > 0:  # If mission picks list is not empty

                mission.set_process(process=1)  # Set mission process to 1

                robot.set_goal(goal=mission.get_picks()[0])

                robot.set_route(
                    route=CreateRoute(vmap, robot.get_pos(), robot.get_goal())
                )

                mission.set_deb_route(
                    deb_route=mission.get_deb_route.append(robot.get_route())
                )

    if mission.get_process == 5:

        if robot.get_pos == mission.get_picks()[0]:

            mission.set_process(process=6)

        if robot.get_pos != mission.get_picks()[0]:

            MoveRobot(robot, mission.get_picks()[0], vmap)

    if mission.get_process == 6:

        # Llamar a csv

        robot.set_goal(goal=[])
        robot.set_route(route=[])
        robot.set_process(process=0)
        robot.set_assignedmission(assignedmission=0)
        robot.set_pos(pos=[])

        mission.set_process(process=7)

    if process == 7:

        return

    return


def MoveRobot(robot, goal, vmap, pick):

    a = 0

    while a == 0:

        if len(robot.get_route()) >= 2:

            # If the robot has 2 or more positions in its route, you check both of them

            r00 = robot.get_route()[0][0]
            r01 = robot.get_route()[0][1]
            r10 = robot.get_route()[1][0]
            r11 = robot.get_route()[1][1]

            if vmap[r00][r01] == 0 and vmap[r10][r11] == 0:

                robot.set_pos(pos=robot.get_route(0))
                robot.set_route(route=robot.get_route().pop(0))

                robot.set_dist(dist=robot.get_dist() + 1)

                a = 1

            elif vmap[r00][r01] == 1 or vmap[r10][r11] == 1:

                vmap2 = copy.deepcopy(vmap)
                vmap2[r00][r01] = 1
                vmap2[r10][r11] = 1

                robot.set_route(CreateRoute(vmap2, robot.get_pos(), goal))

                pick.set_intersections(pick.get_intersections() + 1)

        if len(robot.get_route()) == 1:

            # If the robot has 1 position in its route, you check it

            r00 = robot.get_route()[0][0]
            r01 = robot.get_route()[0][1]

            if vmap[r00][r01] == 0:

                robot.set_pos(pos=robot.get_route(0))
                robot.set_route(route=robot.get_route().pop(0))

                robot.set_dist(dist=robot.get_dist() + 1)

                a = 1

            elif vmap[r00][r01] == 1:

                vmap2 = copy.deepcopy(vmap)
                vmap2[r00][r01] = 1

                robot.set_route(CreateRoute(vmap2, robot.get_pos(), goal))

                pick.set_intersections(pick.get_intersections() + 1)

        if len(robot.get_route()) == 0:

            a = 2  # The robot arrived at the pick or it does not have a route (should not happen)

    return a
