from pathlib import Path
import pandas as pd


def data_CSV(
    ID,
    Numpicks,
    Totaltime,
    Tiempoespera,
    Tiemposalida,
    Misions,
    Distance,
    DistanceMeters,
    Intersections,
):

    # This function creates a new CSV or updates an existing one with the given data.

    dataArray = [
        ID,
        Numpicks,
        Totaltime,
        Tiempoespera,
        Tiemposalida,
        Misions,
        Distance,
        DistanceMeters,
        Intersections,
    ]

    column_names = [  # Name of columns to be declared in csv
        "ID Robot",
        "Number of picks",
        "Tiempo Total",
        "Tiempo espera a humano",
        "Tiempo de salida",
        "Misiones Completadas",
        "Distancia",
        "Distancia en metros",
        "Intersecciones",
    ]

    file_path = Path(
        "/Users/jaimejarauta/Downloads/data.csv"
    )  # The directory should be set for each computer
    if file_path.exists():  # If the file exists, it is updated.
        data = pd.DataFrame([dataArray])
        data.to_csv(file_path, mode="a", index=False,
                    na_rep="Unknown", header=False)
    else:
        # If not, it creates
        data = pd.DataFrame([dataArray], columns=column_names)
        data.to_csv(
            file_path, mode="a", index=False, na_rep="Unknown", header=column_names
        )
