import random
from abc import ABC
import array as arr


class Team(ABC):
    def __init__(self, ID, ELO, G):
        self._ID = ID
        self._ELO = ELO
        self._WC = 0  # World Cup
        self._F: int = 0  # Reach Finals
        self._SF: int = 0  # Reach Semi-Finals
        self._QF: int = 0  # Reach Quarter-Finals
        self._OF: int = 0  # Reach 16th Round
        self._G = G  # Group
        self._W: int = 0  # Win
        self._GID: int = 0  # Group ID
        self._points: int = 0  # Points

    def get_ID(self) -> str:
        return self._ID

    def set_ID(self, ID: str):
        self._ID = ID

    def get_ELO(self) -> float:
        return self._ELO

    def set_ELO(self, ELO: float):
        self._ELO = ELO

    def get_W(self) -> int:
        return self._W

    def set_W(self, W: int):
        self._W = W

    def get_WC(self) -> int:
        return self._WC

    def set_WC(self, WC: int):
        self._WC = WC

    def get_F(self) -> int:
        return self._F

    def set_F(self, F: int):
        self._F = F

    def get_SF(self) -> int:
        return self._SF

    def set_SF(self, SF: int):
        self._SF = SF

    def get_QF(self) -> int:
        return self._QF

    def set_QF(self, QF: int):
        self._QF = QF

    def get_OF(self) -> int:
        return self._OF

    def set_OF(self, OF: int):
        self._OF = OF

    def get_G(self) -> int:
        return self._G

    def set_G(self, G: int):
        self._G = G

    def get_GID(self) -> int:
        return self._GID

    def set_GID(self, GID: int):
        self._GID = GID

    def get_points(self) -> int:
        return self._points

    def set_points(self, points: int):
        self._points = points


t = 0
t2 = 0

VEC = []

VEC.append(Team("NTH", 2050.0, 1))  # 1
VEC.append(Team("ECU", 1871.0, 1))  # 2
VEC.append(Team("SEN", 1667.0, 1))  # 3
VEC.append(Team("QAT", 1642.0, 1))  # 4
VEC.append(Team("ENG", 1957.0, 2))  # 5
VEC.append(Team("USA", 1797.0, 2))  # 6
VEC.append(Team("WAL", 1791.0, 2))  # 7
VEC.append(Team("IRA", 1760.0, 2))  # 8
VEC.append(Team("ARG", 2086.0, 3))  # 9
VEC.append(Team("POL", 1814.0, 3))  # 10
VEC.append(Team("MEX", 1809.0, 3))  # 11
VEC.append(Team("SAU", 1692.0, 3))  # 12
VEC.append(Team("FRA", 2022.0, 4))  # 13
VEC.append(Team("DEN", 1952.0, 4))  # 14
VEC.append(Team("TUN", 1726.0, 4))  # 15
VEC.append(Team("AUS", 1702.0, 4))  # 16
VEC.append(Team("ESP", 2068.0, 5))  # 17
VEC.append(Team("GER", 1919.0, 5))  # 18
VEC.append(Team("JAP", 1831.0, 5))  # 19
VEC.append(Team("CRC", 1723.0, 5))  # 20
VEC.append(Team("BEL", 2007.0, 6))  # 21
VEC.append(Team("CRO", 1914.0, 6))  # 22
VEC.append(Team("MOR", 1779.0, 6))  # 23
VEC.append(Team("CAN", 1776.0, 6))  # 24
VEC.append(Team("BRA", 2169.0, 7))  # 25
VEC.append(Team("SWI", 1902.0, 7))  # 26
VEC.append(Team("SRB", 1898.0, 7))  # 27
VEC.append(Team("CAM", 1610.0, 7))  # 28
VEC.append(Team("POR", 2006.0, 8))  # 29
VEC.append(Team("URU", 1936.0, 8))  # 30
VEC.append(Team("KOR", 1786.0, 8))  # 31
VEC.append(Team("GHA", 1567.0, 8))  # 32

while t < 1000:

    VEC[0].set_ELO(2050.0)
    VEC[1].set_ELO(1871.0)
    VEC[2].set_ELO(1667.0)
    VEC[3].set_ELO(1642.0)
    VEC[4].set_ELO(1957.0)
    VEC[5].set_ELO(1797.0)
    VEC[6].set_ELO(1791.0)
    VEC[7].set_ELO(1760.0)
    VEC[8].set_ELO(2086.0)
    VEC[9].set_ELO(1814.0)
    VEC[10].set_ELO(1809.0)
    VEC[11].set_ELO(1692.0)
    VEC[12].set_ELO(2022.0)
    VEC[13].set_ELO(1952.0)
    VEC[14].set_ELO(1726.0)
    VEC[15].set_ELO(1702.0)
    VEC[16].set_ELO(2068.0)  # 2068
    VEC[17].set_ELO(1919.0)
    VEC[18].set_ELO(1831.0)
    VEC[19].set_ELO(1723.0)
    VEC[20].set_ELO(2007.0)
    VEC[21].set_ELO(1914.0)
    VEC[22].set_ELO(1779.0)
    VEC[23].set_ELO(1776.0)
    VEC[24].set_ELO(2169.0)
    VEC[25].set_ELO(1902.0)
    VEC[26].set_ELO(1898.0)
    VEC[27].set_ELO(1610.0)
    VEC[28].set_ELO(2006.0)
    VEC[29].set_ELO(1936.0)
    VEC[30].set_ELO(1786.0)
    VEC[31].set_ELO(1567.0)

    if t == 0:
        for i in range(0, 31):
            VEC[i].set_W(0)

    for i in range(0, 8):

        ELO1 = VEC[4 * i].get_ELO()
        ELO2 = VEC[4 * i + 1].get_ELO()
        ELO3 = VEC[4 * i + 2].get_ELO()
        ELO4 = VEC[4 * i + 3].get_ELO()

        G1 = 1 / (1 + 10 ** ((ELO2 - ELO1) / 400))
        G2 = 1 / (1 + 10 ** ((ELO4 - ELO3) / 400))

        G3 = 1 / (1 + 10 ** ((ELO3 - ELO1) / 400))
        G4 = 1 / (1 + 10 ** ((ELO4 - ELO2) / 400))

        G5 = 1 / (1 + 10 ** ((ELO4 - ELO1) / 400))
        G6 = 1 / (1 + 10 ** ((ELO3 - ELO2) / 400))

        n = random.random()

        if n < G1:
            NW = VEC[4 * i].get_W() + 1
            VEC[4 * i].set_W(NW)

            NELO = VEC[4 * i].get_ELO() + 32 * (1 - G1)
            VEC[4 * i].set_ELO(NELO)

            NELO = VEC[4 * i + 1].get_ELO() + 32 * (0 - (1 - G1))
            VEC[4 * i + 1].set_ELO(NELO)

            VEC[4 * i].set_points(VEC[4 * i].get_points() + 3)

        else:
            NW = VEC[4 * i + 1].get_W() + 1
            VEC[4 * i + 1].set_W(NW)

            NELO = VEC[4 * i].get_ELO() + 32 * (0 - G1)
            VEC[4 * i].set_ELO(NELO)

            NELO = VEC[4 * i + 1].get_ELO() + 32 * (1 - (1 - G1))
            VEC[4 * i + 1].set_ELO(NELO)

            VEC[4 * i + 1].set_points(VEC[4 * i + 1].get_points() + 3)

        n = random.random()

        if n < G2:
            NW = VEC[4 * i + 2].get_W() + 1
            VEC[4 * i + 2].set_W(NW)

            NELO = VEC[4 * i + 2].get_ELO() + 32 * (1 - G2)
            VEC[4 * i + 2].set_ELO(NELO)

            NELO = VEC[4 * i + 3].get_ELO() + 32 * (0 - (1 - G2))
            VEC[4 * i + 3].set_ELO(NELO)

            VEC[4 * i + 2].set_points(VEC[4 * i + 2].get_points() + 3)

        else:
            NW = VEC[4 * i + 3].get_W() + 1
            VEC[4 * i + 3].set_W(NW)

            NELO = VEC[4 * i + 2].get_ELO() + 32 * (0 - G2)
            VEC[4 * i + 2].set_ELO(NELO)

            NELO = VEC[4 * i + 3].get_ELO() + 32 * (1 - (1 - G2))
            VEC[4 * i + 3].set_ELO(NELO)

            VEC[4 * i + 3].set_points(VEC[4 * i + 3].get_points() + 3)

        n = random.random()

        if n < G3:
            NW = VEC[4 * i].get_W() + 1
            VEC[4 * i].set_W(NW)

            NELO = VEC[4 * i].get_ELO() + 32 * (1 - G3)
            VEC[4 * i].set_ELO(NELO)

            NELO = VEC[4 * i + 2].get_ELO() + 32 * (0 - (1 - G3))
            VEC[4 * i + 2].set_ELO(NELO)

            VEC[4 * i].set_points(VEC[4 * i].get_points() + 3)

        else:
            NW = VEC[4 * i + 2].get_W() + 1
            VEC[4 * i + 2].set_W(NW)

            NELO = VEC[4 * i].get_ELO() + 32 * (0 - G3)
            VEC[4 * i].set_ELO(NELO)

            NELO = VEC[4 * i + 2].get_ELO() + 32 * (1 - (1 - G3))
            VEC[4 * i + 2].set_ELO(NELO)

            VEC[4 * i + 2].set_points(VEC[4 * i + 2].get_points() + 3)

        n = random.random()

        if n < G4:
            NW = VEC[4 * i + 1].get_W() + 1
            VEC[4 * i + 1].set_W(NW)

            NELO = VEC[4 * i + 1].get_ELO() + 32 * (1 - G4)
            VEC[4 * i + 1].set_ELO(NELO)

            NELO = VEC[4 * i + 3].get_ELO() + 32 * (0 - (1 - G4))
            VEC[4 * i + 3].set_ELO(NELO)

            VEC[4 * i + 1].set_points(VEC[4 * i + 1].get_points() + 3)

        else:
            NW = VEC[4 * i + 3].get_W() + 1
            VEC[4 * i + 3].set_W(NW)

            NELO = VEC[4 * i + 1].get_ELO() + 32 * (0 - G4)
            VEC[4 * i + 1].set_ELO(NELO)

            NELO = VEC[4 * i + 3].get_ELO() + 32 * (1 - (1 - G4))
            VEC[4 * i + 3].set_ELO(NELO)

            VEC[4 * i + 3].set_points(VEC[4 * i + 3].get_points() + 3)

        n = random.random()

        if n < G5:
            NW = VEC[4 * i].get_W() + 1
            VEC[4 * i].set_W(NW)

            NELO = VEC[4 * i].get_ELO() + 32 * (1 - G5)
            VEC[4 * i].set_ELO(NELO)

            NELO = VEC[4 * i + 3].get_ELO() + 32 * (0 - (1 - G5))
            VEC[4 * i + 3].set_ELO(NELO)

            VEC[4 * i].set_points(VEC[4 * i].get_points() + 3)

        else:
            NW = VEC[4 * i + 3].get_W() + 1
            VEC[4 * i + 3].set_W(NW)

            NELO = VEC[4 * i].get_ELO() + 32 * (0 - G5)
            VEC[4 * i].set_ELO(NELO)

            NELO = VEC[4 * i + 3].get_ELO() + 32 * (1 - (1 - G5))
            VEC[4 * i + 3].set_ELO(NELO)

            VEC[4 * i + 3].set_points(VEC[4 * i + 3].get_points() + 3)

        n = random.random()

        if n < G6:
            NW = VEC[4 * i + 1].get_W() + 1
            VEC[4 * i + 1].set_W(NW)

            NELO = VEC[4 * i + 1].get_ELO() + 32 * (1 - G6)
            VEC[4 * i + 1].set_ELO(NELO)

            NELO = VEC[4 * i + 2].get_ELO() + 32 * (0 - (1 - G6))
            VEC[4 * i + 2].set_ELO(NELO)

            VEC[4 * i + 1].set_points(VEC[4 * i + 1].get_points() + 3)

        else:
            NW = VEC[4 * i + 2].get_W() + 1
            VEC[4 * i + 2].set_W(NW)

            NELO = VEC[4 * i + 1].get_ELO() + 32 * (0 - G6)
            VEC[4 * i + 1].set_ELO(NELO)

            NELO = VEC[4 * i + 2].get_ELO() + 32 * (1 - (1 - G6))
            VEC[4 * i + 2].set_ELO(NELO)

            VEC[4 * i + 2].set_points(VEC[4 * i + 2].get_points() + 3)

    # Function to see which team has the most points in each group

    VECOF: list = []

    for j in range(0, 16):
        VECOF.append(1)

    for i in range(0, 8):

        P1 = VEC[4 * i].get_points()
        P2 = VEC[4 * i + 1].get_points()
        P3 = VEC[4 * i + 2].get_points()
        P4 = VEC[4 * i + 3].get_points()

        vecpos = []

        vecpos.append(P1)
        vecpos.append(P2)
        vecpos.append(P3)
        vecpos.append(P4)

        # find position of 2 highest numbers in vecpos list

        max1 = max(vecpos)
        max1pos = vecpos.index(max1)
        vecpos[max1pos] = 0
        max2 = max(vecpos)
        max2pos = vecpos.index(max2)

        VEC[4 * i + max1pos].set_OF(VEC[4 * i + max1pos].get_OF() + 1)
        VEC[4 * i + max2pos].set_OF(VEC[4 * i + max2pos].get_OF() + 1)

        if i == 0:  # Group A
            VECOF[0] = 4 * i + max1pos
            VECOF[9] = 4 * i + max2pos

        if i == 1:  # Group B
            VECOF[8] = 4 * i + max1pos
            VECOF[1] = 4 * i + max2pos

        if i == 2:  # Group C
            VECOF[2] = 4 * i + max1pos
            VECOF[11] = 4 * i + max2pos

        if i == 3:  # Group D
            VECOF[10] = 4 * i + max1pos
            VECOF[3] = 4 * i + max2pos

        if i == 4:  # Group E
            VECOF[4] = 4 * i + max1pos
            VECOF[13] = 4 * i + max2pos

        if i == 5:  # Group F
            VECOF[12] = 4 * i + max1pos
            VECOF[5] = 4 * i + max2pos

        if i == 6:  # Group G
            VECOF[6] = 4 * i + max1pos
            VECOF[15] = 4 * i + max2pos

        if i == 7:  # Group H
            VECOF[14] = 4 * i + max1pos
            VECOF[7] = 4 * i + max2pos

    # Play games between OF1 and OF2, OF3 and OF4, etc.

    VECQF: list = []

    for i in range(0, 8):

        ELO1 = VEC[VECOF[2 * i]].get_ELO()
        ELO2 = VEC[VECOF[2 * i + 1]].get_ELO()

        G1 = 1 / (1 + 10 ** ((ELO2 - ELO1) / 400))

        n = random.random()

        if n < G1:
            NW = VEC[VECOF[2 * i]].get_W() + 1
            VEC[VECOF[2 * i]].set_W(NW)

            NELO = VEC[VECOF[2 * i]].get_ELO() + 32 * (1 - G1)
            VEC[VECOF[2 * i]].set_ELO(NELO)

            NELO = VEC[VECOF[2 * i + 1]].get_ELO() + 32 * (0 - (1 - G1))
            VEC[VECOF[2 * i + 1]].set_ELO(NELO)

            VEC[VECOF[2 * i]].set_points(VEC[VECOF[2 * i]].get_points() + 3)

            VEC[VECOF[2 * i]].set_QF(VEC[VECOF[2 * i]].get_QF() + 1)

            VECQF.append(VECOF[2 * i])

        else:
            NW = VEC[VECOF[2 * i + 1]].get_W() + 1
            VEC[VECOF[2 * i + 1]].set_W(NW)

            NELO = VEC[VECOF[2 * i]].get_ELO() + 32 * (0 - G1)
            VEC[VECOF[2 * i]].set_ELO(NELO)

            NELO = VEC[VECOF[2 * i + 1]].get_ELO() + 32 * (1 - (1 - G1))
            VEC[VECOF[2 * i + 1]].set_ELO(NELO)

            VEC[VECOF[2 * i + 1]].set_points(VEC[VECOF[2 * i + 1]].get_points() + 3)

            VEC[VECOF[2 * i + 1]].set_QF(VEC[VECOF[2 * i + 1]].get_QF() + 1)

            VECQF.append(VECOF[2 * i + 1])

    # Play games between QF1 and QF2, QF3 and QF4, etc.

    VECSF: list = []

    for i in range(0, 4):

        ELO1 = VEC[VECQF[2 * i]].get_ELO()
        ELO2 = VEC[VECQF[2 * i + 1]].get_ELO()

        G1 = 1 / (1 + 10 ** ((ELO2 - ELO1) / 400))

        n = random.random()

        if n < G1:
            NW = VEC[VECQF[2 * i]].get_W() + 1
            VEC[VECQF[2 * i]].set_W(NW)

            NELO = VEC[VECQF[2 * i]].get_ELO() + 32 * (1 - G1)
            VEC[VECQF[2 * i]].set_ELO(NELO)

            NELO = VEC[VECQF[2 * i + 1]].get_ELO() + 32 * (0 - (1 - G1))
            VEC[VECQF[2 * i + 1]].set_ELO(NELO)

            VEC[VECQF[2 * i]].set_points(VEC[VECQF[2 * i]].get_points() + 3)

            VEC[VECQF[2 * i]].set_SF(VEC[VECQF[2 * i]].get_SF() + 1)

            VECSF.append(VECQF[2 * i])

        else:
            NW = VEC[VECQF[2 * i + 1]].get_W() + 1
            VEC[VECQF[2 * i + 1]].set_W(NW)

            NELO = VEC[VECQF[2 * i]].get_ELO() + 32 * (0 - G1)
            VEC[VECQF[2 * i]].set_ELO(NELO)

            NELO = VEC[VECQF[2 * i + 1]].get_ELO() + 32 * (1 - (1 - G1))
            VEC[VECQF[2 * i + 1]].set_ELO(NELO)

            VEC[VECQF[2 * i + 1]].set_points(VEC[VECQF[2 * i + 1]].get_points() + 3)

            VEC[VECQF[2 * i + 1]].set_SF(VEC[VECQF[2 * i + 1]].get_SF() + 1)

            VECSF.append(VECQF[2 * i + 1])

        # Play games between SF1 and SF2

    VECF: list = []

    for i in range(0, 2):

        ELO1 = VEC[VECSF[0]].get_ELO()
        ELO2 = VEC[VECSF[1]].get_ELO()

        G1 = 1 / (1 + 10 ** ((ELO2 - ELO1) / 400))

        n = random.random()

        if n < G1:
            NW = VEC[VECSF[0]].get_W() + 1
            VEC[VECSF[0]].set_W(NW)

            NELO = VEC[VECSF[0]].get_ELO() + 32 * (1 - G1)
            VEC[VECSF[0]].set_ELO(NELO)

            NELO = VEC[VECSF[1]].get_ELO() + 32 * (0 - (1 - G1))
            VEC[VECSF[1]].set_ELO(NELO)

            VEC[VECSF[0]].set_points(VEC[VECSF[0]].get_points() + 3)

            VEC[VECSF[0]].set_F(VEC[VECSF[0]].get_F() + 1)

            VECF.append(VECSF[0])

        else:
            NW = VEC[VECSF[1]].get_W() + 1
            VEC[VECSF[1]].set_W(NW)

            NELO = VEC[VECSF[0]].get_ELO() + 32 * (0 - G1)
            VEC[VECSF[0]].set_ELO(NELO)

            NELO = VEC[VECSF[1]].get_ELO() + 32 * (1 - (1 - G1))
            VEC[VECSF[1]].set_ELO(NELO)

            VEC[VECSF[1]].set_points(VEC[VECSF[1]].get_points() + 3)

            VEC[VECSF[1]].set_F(VEC[VECSF[1]].get_F() + 1)

            VECF.append(VECSF[1])

    # Play games between F1 and F2

    ELO1 = VEC[VECF[0]].get_ELO()
    ELO2 = VEC[VECF[1]].get_ELO()

    G1 = 1 / (1 + 10 ** ((ELO2 - ELO1) / 400))

    n = random.random()

    if n < G1:
        NW = VEC[VECF[0]].get_W() + 1
        VEC[VECF[0]].set_W(NW)

        NELO = VEC[VECF[0]].get_ELO() + 32 * (1 - G1)
        VEC[VECF[0]].set_ELO(NELO)

        NELO = VEC[VECF[1]].get_ELO() + 32 * (0 - (1 - G1))
        VEC[VECF[1]].set_ELO(NELO)

        VEC[VECF[0]].set_points(VEC[VECF[0]].get_points() + 3)

        VEC[VECF[0]].set_WC(VEC[VECF[0]].get_WC() + 1)
        # print("T: ", t, "Win: ", VEC[VECF[0]].get_ID())

    else:
        NW = VEC[VECF[1]].get_W() + 1
        VEC[VECF[1]].set_W(NW)

        NELO = VEC[VECF[0]].get_ELO() + 32 * (0 - G1)
        VEC[VECF[0]].set_ELO(NELO)

        NELO = VEC[VECF[1]].get_ELO() + 32 * (1 - (1 - G1))
        VEC[VECF[1]].set_ELO(NELO)

        VEC[VECF[1]].set_points(VEC[VECF[1]].get_points() + 3)

        VEC[VECF[1]].set_WC(VEC[VECF[1]].get_WC() + 1)

        # print("T: ", t, "Win: ", VEC[VECF[1]].get_ID())

    t = t + 1

# Find team with most WC

max = 0
for i in range(0, 32):
    if VEC[i].get_WC() > max:
        max = VEC[i].get_WC()
        max_index = i

print(
    "The winner is: ", VEC[max_index].get_ID(), "with", VEC[max_index].get_WC(), "wins"
)

# Print list ordered by most world cup wins

VEC.sort(key=lambda x: x.get_WC(), reverse=True)

for i in range(0, 32):

    if VEC[i].get_WC() > 0:

        print(
            "Team: ",
            VEC[i].get_ID(),
            "World Cup Wins: ",
            VEC[i].get_WC(),
        )
