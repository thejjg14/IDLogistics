from pathlib import Path
import pandas as pd


def data_CSV(robot):

    # This function creates a new CSV or updates an existing one with the given data.
    for i in range(0, len(robot.get_picksdone()) - 1):
        dataArray = [
            robot.get_ID(),
            robot.get_pos(),
            robot.get_route(),
            robot.get_dist(),
            robot.get_assignedmission(),
            robot.get_downloadtime(),
        ]

        column_names = [  # Name of columns to be declared in csv
            "ID Robot",
            "Posición",
            "Ruta",
            "Distancia",
            "Misión Asignada",
            "Tiempo de descarga",
        ]

        file_path = Path("data.csv")  # The directory should be set for each computer
        if file_path.exists():  # If the file exists, it is updated.
            data = pd.DataFrame([dataArray])
            data.to_csv(
                file_path, mode="a", index=False, na_rep="Unknown", header=False
            )
        else:
            # If not, it creates
            data = pd.DataFrame([dataArray], columns=column_names)
            data.to_csv(
                file_path, mode="a", index=False, na_rep="Unknown", header=column_names
            )
