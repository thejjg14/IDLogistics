# Python Simulation of Chaotic Warehouse Operations

## Project Description
This Python-based simulation offers a detailed analysis of chaotic warehouse operations involving both humans and autonomous "Locus" Robots. The simulation specifically addresses unit picking operations within an e-commerce distribution warehouse, focusing on optimizing the collaboration between human workers and robots. It models various scenarios within a dynamically changing warehouse environment, such as pathfinding, task allocation, and interaction dynamics to enhance operational efficiency and workflow management. This simulation can serve as a powerful tool for logistics planners and engineers seeking to understand and improve warehouse operations.

The simulation framework sets up a realistic warehouse environment where multiple robots and human operators work in tandem. The system is designed to handle numerous autonomous robots and a varied number of human operators, reflecting typical mid to large-scale operations. The simulator tracks and analyzes the efficiency of robots and humans, focusing on metrics such as time to pick, distances traveled, and task completion rates.

By leveraging advanced algorithms for route optimization and task allocation, the simulation provides insights into the optimal number of robots and humans needed to achieve maximum efficiency. It simulates different time of day operations, from peak hours to off-peak hours, allowing for detailed planning and scheduling. The customizable settings enable users to adjust parameters such as warehouse size, number of aisles, and specific task assignments to closely mimic their operational conditions.

## Authors
- Jaime Jarauta Gastelu
- Galo Iglesias Aramburu

### Co-Authors
- Juan Pérez Vilanova
- María Paloma Vázquez Pérez-Palencia
- Álvaro Ruiz Cabrera

## Latest Version
V3.2 (May 2023)

## Date
2022-2023

## Installation

### Prerequisites
Ensure you have Python 3.x installed on your machine. You can download it from [python.org](https://www.python.org/downloads/).

### Dependencies
The project requires the following Python libraries:
- NumPy
- Matplotlib
- Pandas
- Tkinter

You can install all required packages using the following command:
```bash
pip install numpy matplotlib pandas
