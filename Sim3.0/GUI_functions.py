import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showinfo

#############################
#     Initial Interface
#############################


def ini_GUI():
    initial_window = tk.Tk()
    initial_window.title("ID Logistics - Warehouse Simulation")

    data_intro_frame = tk.LabelFrame(
        initial_window, text="ID Logistics - Warehouse Simulation", padx=20, pady=20
    )
    data_intro_frame.grid(row=10, column=10, padx=10, pady=10, columnspan=3)

    def new_simulation():
        global selection
        selection = 1
        initial_window.destroy()

    def search_simulation():
        global selection
        selection = 2
        initial_window.destroy()

    def exit_simulation():
        global selection
        selection = 0
        initial_window.destroy()

    # New Simulation Button
    new_sim_button = tk.Button(
        data_intro_frame, text="New Simulation", fg="blue", command=new_simulation
    )
    new_sim_button.grid(row=1, column=0, pady=10, columnspan=1)

    # Search Simulation Button
    search_sim_button = tk.Button(
        data_intro_frame,
        text="Search for Simulation",
        fg="blue",
        command=search_simulation,
    )
    search_sim_button.grid(row=2, column=0, pady=10, columnspan=1)

    # Exit Button
    exit_button = tk.Button(
        data_intro_frame, text="Exit", fg="red", command=exit_simulation
    )
    exit_button.grid(row=3, column=0, pady=10, columnspan=1)

    # FUNCIONAMIENTO CONTINUO DE LA INTERFAZ
    initial_window.mainloop()

    return selection


#############################
# Data introduction interface
#############################


def data_intro_GUI():
    data_introduction_window = tk.Tk()
    data_introduction_window.title(
        "ID Logistics - Warehouse Simulation - Data Introduction"
    )

    data_intro_frame = tk.LabelFrame(
        data_introduction_window, text="Data Introduction", padx=20, pady=20
    )
    data_intro_frame.grid(row=10, column=10, padx=10, pady=10, columnspan=3)

    def save_data():
        global selection_map
        global Num_Robots
        global Num_Humans
        global High_Demand_Days
        global Low_Demand_Days
        global Num_order_mission_min
        global Num_order_mission_max
        global Num_article_order_min
        global Num_article_order_max
        global T_Wave
        global Num_Mission_Wave_min
        global Num_Mission_Wave_max
        global selection

        selection_map = click_map.get()
        Num_Robots = entry_num_robots.get()
        Num_Humans = entry_num_humans.get()
        High_Demand_Days = entry_High_Demand.get()
        Low_Demand_Days = entry_Low_Demand.get()
        Num_order_mission_min = entry_Order_Mission_min.get()
        Num_order_mission_max = entry_Order_Mission_max.get()
        Num_article_order_min = entry_Articles_Orders_min.get()
        Num_article_order_max = entry_Articles_Orders_max.get()
        T_Wave = entry_T_Waves.get()
        Num_Mission_Wave_min = entry_Mission_Wave_min.get()
        Num_Mission_Wave_max = entry_Mission_Wave_max.get()
        selection = 1

        data_introduction_window.destroy()

    def exit_simulation():
        global selection

        selection = 0

        data_introduction_window.destroy()

    # Title

    Label_map_title = tk.Label(data_intro_frame, text="DATA INTRODUCTION")
    Label_map_title.grid(row=0, column=0, padx=10, pady=10, columnspan=6)

    # Map Selection

    menu_options_map = [
        "Seseña",
        "Tarancón SPF",
        "Tarancón CTF",
    ]

    Label_map_title = tk.Label(data_intro_frame, text="Map Selection")
    Label_map_title.grid(row=1, column=0, padx=10, pady=10)

    click_map = tk.StringVar()
    click_map.set(menu_options_map[0])
    menu_map = tk.OptionMenu(data_intro_frame, click_map, *menu_options_map)
    menu_map.grid(row=2, column=0)

    # Number of robots selection

    Label_agent_title = tk.Label(data_intro_frame, text="Agents")
    Label_agent_title.grid(row=1, column=1, padx=10, pady=10)

    Label_Robot = tk.Label(data_intro_frame, text="Number of robots")
    Label_Robot.grid(row=2, column=1, padx=10)
    entry_num_robots = tk.Entry(data_intro_frame, width=15)
    entry_num_robots.grid(row=2, column=2)

    # Number of humans selection

    Label_Humans = tk.Label(data_intro_frame, text="Number of humans")
    Label_Humans.grid(row=3, column=1, padx=10)
    entry_num_humans = tk.Entry(data_intro_frame, width=15)
    entry_num_humans.grid(row=3, column=2)

    # Demand Selection

    Label_demand_title = tk.Label(data_intro_frame, text="Demand")
    Label_demand_title.grid(row=1, column=3, padx=10, pady=10)

    Label_High_Demand = tk.Label(data_intro_frame, text="High demand days: ")
    Label_High_Demand.grid(row=2, column=3, padx=10, pady=10)
    entry_High_Demand = tk.Entry(data_intro_frame, width=15)
    entry_High_Demand.grid(row=2, column=4)

    Label_Low_Demand = tk.Label(data_intro_frame, text="Low demand days: ")
    Label_Low_Demand.grid(row=3, column=3, padx=10)
    entry_Low_Demand = tk.Entry(data_intro_frame, width=15)
    entry_Low_Demand.grid(row=3, column=4)

    # Order Selection

    Label_order_title = tk.Label(data_intro_frame, text="Orders")
    Label_order_title.grid(row=4, column=0, padx=10, pady=10)

    Label_Order_Mission = tk.Label(data_intro_frame, text="Orders per mission: ")
    Label_Order_Mission.grid(row=5, column=0, pady=10)
    entry_Order_Mission_min = tk.Entry(data_intro_frame, width=15)
    entry_Order_Mission_min.grid(row=5, column=1)
    entry_Order_Mission_max = tk.Entry(data_intro_frame, width=15)
    entry_Order_Mission_max.grid(row=5, column=2)

    Label_Articles_Orders = tk.Label(data_intro_frame, text="Articles per order: ")
    Label_Articles_Orders.grid(row=6, column=0)
    entry_Articles_Orders_min = tk.Entry(data_intro_frame, width=15)
    entry_Articles_Orders_min.grid(row=6, column=1)
    entry_Articles_Orders_max = tk.Entry(data_intro_frame, width=15)
    entry_Articles_Orders_max.grid(row=6, column=2)

    # Number of waves selection

    Label_waves_title = tk.Label(data_intro_frame, text="Waves")
    Label_waves_title.grid(row=4, column=3, padx=10, pady=10)

    Label_T_Waves = tk.Label(data_intro_frame, text="Time between waves (h): ")
    Label_T_Waves.grid(row=5, column=3, padx=20)
    entry_T_Waves = tk.Entry(data_intro_frame, width=15)
    entry_T_Waves.grid(row=5, column=4)

    Label_Mission_Wave = tk.Label(data_intro_frame, text="Mission per wave: ")
    Label_Mission_Wave.grid(row=6, column=3)
    entry_Mission_Wave_min = tk.Entry(data_intro_frame, width=15)
    entry_Mission_Wave_min.grid(row=6, column=4, padx=0)
    entry_Mission_Wave_max = tk.Entry(data_intro_frame, width=15)
    entry_Mission_Wave_max.grid(row=6, column=5, padx=20)

    # Save Data
    save_map_button = tk.Button(
        data_intro_frame, text="Start Simulation", fg="blue", command=save_data
    )
    save_map_button.grid(row=8, column=0, pady=30, columnspan=4)

    # Exit
    exit_button = tk.Button(
        data_intro_frame, text="Exit Simulation", fg="red", command=exit_simulation
    )
    exit_button.grid(row=8, column=3, pady=30, columnspan=1)

    # FUNCIONAMIENTO CONTINUO DE LA INTERFAZ
    data_introduction_window.mainloop()

    if selection:
        initial_parameters = {
            "Selection_map": selection_map,
            "Num_Robots": Num_Robots,
            "Num_Humans": Num_Humans,
            "High_Demand_Days": High_Demand_Days,
            "Low_Demand_Days": Low_Demand_Days,
            "Num_order_mission_min": Num_order_mission_min,
            "Num_order_mission_max": Num_order_mission_max,
            "Num_article_order_min": Num_article_order_min,
            "Num_article_order_max": Num_article_order_max,
            "T_Wave": T_Wave,
            "Num_Mission_Wave_min": Num_Mission_Wave_min,
            "Num_Mission_Wave_max": Num_Mission_Wave_max,
            "Selection": selection,
        }
    else:
        initial_parameters = {"Selection": selection}

    return initial_parameters


'''
######################
# Execution Interface
######################


class ExecutionParam:
    def __init__(self) -> None:
        self.day = 0
        self.hour = 0
        self.minute = 0
        self.waves = 0
        self.missions = 0
        self.orders = 0

    def set_time(self, day, hour, minute):
        self.day = day
        self.hour = hour
        self.minute = minute

    def set_completed(self, waves, missions, orders):
        self.waves = waves
        self.missions = missions
        self.orders = orders


"""
def set_time(day, hour, minute):
    execution_param.set_time_class(day, hour, minute)
"""


def update_day_label(day_sub_frm, execution_param):
    day = str(execution_param.day)
    Label_day_value = tk.Label(day_sub_frm, text="1000", width=10)
    Label_day_value.grid(row=0, column=0, pady=2)


def update_hour_label(hour_sub_frm, execution_param):
    hour = str(execution_param.hour)
    Label_hour_value = tk.Label(hour_sub_frm, text=hour, width=10)
    Label_hour_value.grid(row=0, column=0, pady=2)


def update_minute_label(minute_sub_frm, execution_param):
    minute = str(execution_param.minute)
    Label_minute_value = tk.Label(minute_sub_frm, text=minute, width=10)
    Label_minute_value.grid(row=0, column=0, pady=2)


# Time update functions
def update_waves_label(execution_param):
    waves = str(execution_param.waves)
    return waves


def update_missions_label(execution_param):
    missions = str(execution_param.missions)
    return missions


def update_orders_label(execution_param):
    orders = str(execution_param.orders)
    return orders


"""
def set_completed(waves, missions, orders):
    execution_param.waves = waves
    execution_param.missions = missions
    execution_param.orders = orders
"""


def execution_GUI(execution_window, execution_param):
    execution_window.title("ID Logistics - Warehouse Simulation - Simulating")

    execution_frame = tk.LabelFrame(
        execution_window, text="Simulating", padx=20, pady=20
    )
    execution_frame.grid(row=0, column=0, padx=10, pady=10, columnspan=3)

    # Title

    Simulating_title = tk.Label(execution_frame, text="Simulating")
    Simulating_title.grid(row=0, column=0, padx=10, pady=10, columnspan=4)

    # Running Time

    Label_day_title = tk.Label(execution_frame, text="Day")
    Label_day_title.grid(row=1, column=1, padx=15, pady=10)
    Label_hour_title = tk.Label(execution_frame, text="Hour")
    Label_hour_title.grid(row=1, column=2, padx=15, pady=10)
    Label_minute_title = tk.Label(execution_frame, text="Minute")
    Label_minute_title.grid(row=1, column=3, padx=15, pady=10)
    Label_time_title = tk.Label(execution_frame, text="Running Time: ")
    Label_time_title.grid(row=2, column=0, padx=5, pady=5, sticky="w")

    # Subframe for day info
    day_sub_frm = tk.Frame(execution_frame, width=20, relief=tk.SUNKEN, borderwidth=2)
    day_sub_frm.grid(row=2, column=1, padx=10, pady=2)
    Label_day_value = tk.Label(day_sub_frm, text=str(execution_param.day), width=10)
    Label_day_value.grid(row=0, column=0, pady=2)

    # Subframe for hour info
    hour_sub_frm = tk.Frame(execution_frame, width=20, relief=tk.SUNKEN, borderwidth=2)
    hour_sub_frm.grid(row=2, column=2, padx=10, pady=2)
    Label_hour_value = tk.Label(hour_sub_frm, text=str(execution_param.hour), width=10)
    Label_hour_value.grid(row=0, column=0, pady=2)

    # Subframe for minute info
    minute_sub_frm = tk.Frame(
        execution_frame, width=20, relief=tk.SUNKEN, borderwidth=2
    )
    minute_sub_frm.grid(row=2, column=3, padx=10, pady=2)
    Label_minute_value = tk.Label(
        minute_sub_frm, text=str(execution_param.minute), width=10
    )
    Label_minute_value.grid(row=0, column=0, pady=2)

    # Progress bar

    # Subframe for time info
    pb_sub_frm = tk.Frame(execution_frame, width=20, relief=tk.SUNKEN, borderwidth=0)
    pb_sub_frm.grid(row=3, column=0, padx=10, pady=15, columnspan=4)

    # PB update functions
    def update_progress_label():
        progressbar["value"] = 100
        return f"Current Progress: {progressbar['value']}%"

    def progress():
        if progressbar["value"] < 100:
            progressbar["value"] += 20
            label_pb["text"] = update_progress_label()
        else:
            label_pb["text"] = "Simulation Completed"

    def stop():
        progressbar.stop()
        label_pb["text"] = "Simulation Completed"

    progressbar = ttk.Progressbar(
        pb_sub_frm, orient="horizontal", mode="determinate", length=400
    )
    progressbar.grid(row=0, column=0, padx=10, pady=15)

    label_pb = ttk.Label(pb_sub_frm, text=update_progress_label())
    label_pb.grid(row=1, column=0, pady=5)

    # Simulation Info

    Label_waves = tk.Label(execution_frame, text="Waves completed: ")
    Label_waves.grid(row=5, column=0, padx=5, pady=5, sticky="w")
    Label_missions = tk.Label(execution_frame, text="Missions completed: ")
    Label_missions.grid(row=6, column=0, padx=5, pady=5, sticky="w")
    Label_orders = tk.Label(execution_frame, text="Orders completed: ")
    Label_orders.grid(row=7, column=0, padx=5, pady=5, sticky="w")

    # Subframe for waves info
    waves_sub_frm = tk.Frame(execution_frame, width=20, relief=tk.SUNKEN, borderwidth=2)
    waves_sub_frm.grid(row=5, column=1, padx=10, pady=2)
    Label_waves_value = tk.Label(
        waves_sub_frm, text=update_waves_label(execution_param), width=10
    )
    Label_waves_value.grid(row=0, column=0, pady=2)

    # Subframe for missions info
    missions_sub_frm = tk.Frame(
        execution_frame, width=20, relief=tk.SUNKEN, borderwidth=2
    )
    missions_sub_frm.grid(row=6, column=1, padx=10, pady=2)
    Label_missions_value = tk.Label(
        missions_sub_frm, text=update_missions_label(execution_param), width=10
    )
    Label_missions_value.grid(row=0, column=0, pady=2)

    # Subframe for orders info
    orders_sub_frm = tk.Frame(
        execution_frame, width=20, relief=tk.SUNKEN, borderwidth=2
    )
    orders_sub_frm.grid(row=7, column=1, padx=10, pady=2)
    Label_orders_value = tk.Label(
        orders_sub_frm, text=update_orders_label(execution_param), width=10
    )
    Label_orders_value.grid(row=0, column=0, pady=2)

    # Next Window Function
    def next():
        if progressbar["value"] == 100:
            execution_window.destroy()

    # Next Window Button
    next_window_button = tk.Button(
        execution_frame, text="Next", fg="blue", command=next
    )
    next_window_button.grid(row=8, column=0, pady=30, columnspan=6)

    # Time update functions

    # update day info
    day_sub_frm.after(0, update_day_label(day_sub_frm, execution_param))

    # update for hour info
    hour_sub_frm.after(0, update_hour_label(hour_sub_frm, execution_param))

    # update for minute info
    minute_sub_frm.after(0, update_minute_label(minute_sub_frm, execution_param))
'''
